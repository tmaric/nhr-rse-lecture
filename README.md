# nhr-rse-lecture

This is the repository of the Research Software Engineering lecture at the [NHR Graduate School](https://www.nhr-gs.de/ueber-uns/nhr-graduiertenschule)  

## Description

The lecture covers a Research Software Engineering Workflow... 

## Getting started

## Installation

**OpenFOAM-v2112**

* Clone [OpenFOAM-v2112](https://develop.openfoam.com/Development/openfoam/) and check-out the latest release tag

```
    > mkdir -p $HOME/OpenFOAM && cd $HOME/OpenFOAM
    > git clone https://develop.openfoam.com/Development/openfoam.git 
    > cd openfoam && git checkout OpenFOAM-v2112
```

To speed up compiling, increase the number of compilation threads

```
    > export WM_NCOMPPROCS=4 
```

Compile OpenFOAM 

```
    > ./Allwmake 
```

OpenFOAM uses Open MPI available on the system and this also works on HPC clusters that use modules. Once OpenFOAM is compiled source the setup script to define its environment variables 

```
    > source $HOME/OpenFOAM/openfoam/etc/bashrc
```

**Custom Preconditioned Conjugate Gradient - customPCG**

Once OpenFOAM is installed, run 

```
    nhr-rse-lecture> ./Allwmake
```

### Compiling OpenFOAM in Prof mode 

Edit `openfoam/etc/bashrc`

```
# [WM_COMPILE_OPTION] - Optimised(default), debug, profiling, other:
# = Opt | Debug | Prof
# Other is processor or packaging specific (eg, OptKNL)
export WM_COMPILE_OPTION=Opt
```

**ParaView**

## Usage

The validation case is placed in `cases/cavity`, the simulation is executed with 

```
    > ./Allrun 
```

in serial, and with 

```
    > ./Allrun.parallel 
```

in parallel (locally) with 4 MPI processes. 

The domain decomposition is defined and described in `system/decomposeParDict`. Once `system/decomposeParDict` is modified, run  

```
    > decomposePar -force 
```

to perform domain decomposition. Parallel solver execution 

```
    > mpirun -np 4 icoFoam -parallel 
```

requires the `-parallel` option, which is relevant also for a workload manager job submission.

Mesh resolution is defined in `system/blockMeshDict` 

```
blocks
(
    hex (0 1 2 3 4 5 6 7) (128 128 1) simpleGrading (1 1 1)
);
```

the case is 2D, discretized with 128 x 128 finite volumes. 

Simulation control is defined in `system/controlDict`, relevant parameter is the simulation time, much longer for verification, than for performance engineering  

```
// ******** Validation
//endTime         10; 
//writeControl    timeStep;
//writeInterval   100; 
// ******** Validation

// ******** Performance Engineering 
endTime         1; 
writeControl    runTime;
writeInterval   1; 
// ******** Performance Engineering 
```

## Performance Engineering 

The `icoFoam` solver discretizes incompressible single-phase Navier-Stokes equations using an implicit unstructured collocated Finite Volume Method which results in a sparse linear system. The linear system solution settings are in  

```
    cases/cavity/system/fvSolution
```

A segregated solution algorithm is used to couple the momentum conservation equation, the pressure equation and the discrete volume conservation equation (zero-sum for volumetric flux).

Conjugate Gradient (PCG - P for Preconditioned) is chosen for the linear solver for the pressure equation, and the preconditioning is disabled for performance engineering. OpenFOAM uses Runtime Type Selection, a Build Pattern that maps virtual member functions that clone objects (solvers, discretizations, the mesh, etc.), with the human-readable names of these objects (`PCG` in `system/fvSolution`). To find out what else is available use 


```
    "(p|psi|pFinal)"
    {
        solver          test;
        preconditioner  test;
```

any other keyword can be used, as long as it's not already in the RTS hashtable, in that case, a solver matching the key(word) will be selected when `icoFoam` is used in the command line. Using `test` in configuration files will output a _RTS table_ with available options. 

The solver `icoFoam` implementation is in `$FOAM_SOLVERS/incompressible` and the PCG linear solver implementation is in `$FOAM_SRC/OpenFOAM/matrices/lduMatrix/solvers/PCG/`.

The OpenFOAM's LDU matrix addressing format is described in [1], in section "7.2.4 lduAddressing", and the CG solver is described in [1], and the linear solver organizational structure is described in [1], section "10.5.2 OpenFOAM".

[1] Moukalled, F., Mangani, L., & Darwish, M. (2016). The finite volume method. In The finite volume method in computational fluid dynamics (pp. 103-135). Springer, Cham.

## Research Software Engineering 

## Acknowledgment

July 1 2020 - June 2024 Funded by the German Research Foundation (DFG) – Project-ID 265191195 – SFB 1194

## License

Licensed under [Gnu General Public License v3.0 or later](https://www.gnu.org/licenses/gpl-3.0.en.html).
